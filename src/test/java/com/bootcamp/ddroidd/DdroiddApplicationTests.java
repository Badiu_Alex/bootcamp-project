package com.bootcamp.ddroidd;

import com.bootcamp.ddroidd.dtos.ApplicationDTO;
import com.bootcamp.ddroidd.entities.Applicant;
import com.bootcamp.ddroidd.entities.Application;
import com.bootcamp.ddroidd.entities.JobListing;
import com.bootcamp.ddroidd.repository.ApplicationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
class DdroiddApplicationTests {

    @MockBean
    private ApplicationRepository applicationRepository;

    private ApplicationDTO applicationDTO;

    private Application application;

    private Applicant applicant;

    private JobListing jobListing;

    @BeforeEach
    private void setup() {
        MockitoAnnotations.openMocks(this);
        this.applicant = createApplicant();
        this.jobListing = createJobListing();
        this.application = createApplicationObject();
        this.applicationDTO = createApplicationDTO();
    }

    private ApplicationDTO createApplicationDTO() {
        applicationDTO = new ApplicationDTO();
        applicationDTO.setId(1);
        applicationDTO.setApplicantId(1);
        applicationDTO.setJobListingId(1);
        return applicationDTO;
    }

    private Applicant createApplicant() {
        applicant = new Applicant();
        applicant.setId(1);
        return applicant;
    }

    private JobListing createJobListing() {
        jobListing = new JobListing();
        jobListing.setId(1);
        return jobListing;
    }

    private Application createApplicationObject() {
        application = new Application();
        application.setId(1);
        application.setApplicant(applicant);
        application.setJobListing(jobListing);
        return application;
    }

    @Test
    void createApplication() {
        when(applicationRepository.save(application)).thenReturn(application);
        assertEquals(1, application.getId());
    }

}
