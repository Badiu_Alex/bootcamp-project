package com.bootcamp.ddroidd.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Employer {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private String domain;

    private String description;

    @OneToMany(mappedBy = "employer")
    private Set<JobListing> jobListings;

}
