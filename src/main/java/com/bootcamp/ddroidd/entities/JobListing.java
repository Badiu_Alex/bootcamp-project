package com.bootcamp.ddroidd.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

@Entity
@Getter
@Setter
public class JobListing implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    private String position;

    private String description;

    @ManyToOne
    @JoinColumn
    private Employer employer;

    @OneToMany(mappedBy = "jobListing")
    private Set<Application> applications;

}
