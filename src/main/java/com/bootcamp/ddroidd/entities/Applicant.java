package com.bootcamp.ddroidd.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Applicant {

    @Id
    @GeneratedValue
    private Integer id;

    private String firstName;

    private String lastName;

    private String phone;

    private String email;

    private String address;

    private String country;

    private String state;

    @OneToMany(mappedBy = "applicant")
    private Set<Application> applications;

}
