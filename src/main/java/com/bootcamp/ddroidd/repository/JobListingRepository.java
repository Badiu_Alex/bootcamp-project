package com.bootcamp.ddroidd.repository;

import com.bootcamp.ddroidd.entities.JobListing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface JobListingRepository extends JpaRepository<JobListing, Integer> {

    List<JobListing> findAllByEmployer_Id(Integer employerId);

    Optional<JobListing> findByPosition(String position);

    @Query("SELECT jl.id FROM JobListing jl WHERE jl.employer.id=:employerId")
    List<Integer> findJobListingsIDsByEmployerId(@Param("employerId") Integer employerId);

}
