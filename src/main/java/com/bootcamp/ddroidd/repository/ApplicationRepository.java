package com.bootcamp.ddroidd.repository;

import com.bootcamp.ddroidd.entities.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ApplicationRepository extends JpaRepository<Application, Integer> {

    @Query("SELECT applicant.id FROM Application WHERE jobListing.id IN :jobListingIds")
    List<Integer> findApplicantsIdsByJobListingIds(@Param("jobListingIds") List<Integer> jobListingIds);

    @Query("SELECT applicant.id FROM Application WHERE jobListing.id = :jobListingId")
    List<Integer> findApplicantsIdsByJobListingId(@Param("jobListingId") Integer jobListingId);

    @Query("SELECT id FROM Application WHERE applicant.id = :applicantId AND jobListing.id = :jobListingId")
    List<Integer> findApplicationsIdsByApplicantIdAndJobListingId(@Param("applicantId") Integer applicantId, @Param("jobListingId") Integer jobListingId);

}
