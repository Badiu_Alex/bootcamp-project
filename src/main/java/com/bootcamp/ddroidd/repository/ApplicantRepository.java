package com.bootcamp.ddroidd.repository;

import com.bootcamp.ddroidd.entities.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ApplicantRepository extends JpaRepository<Applicant, Integer> {
    Optional<Applicant> findByEmail(String email);

    @Query("SELECT a FROM Applicant a WHERE a.id IN :ids")
    List<Applicant> findApplicantsByIds(@Param("ids") List<Integer> ids);

}
