package com.bootcamp.ddroidd.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApplicationDTO {

    private Integer id;

    private Integer applicantId;

    private Integer jobListingId;

}
