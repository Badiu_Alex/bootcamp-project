package com.bootcamp.ddroidd.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EmployerDTO {

    private Integer id;

    private String name;

    private String domain;

    private String description;

}
