package com.bootcamp.ddroidd.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApplicantDTO {

    private Integer id;

    private String firstName;

    private String lastName;

    private String phone;

    private String email;

    private String address;

    private String country;

    private String state;

}
