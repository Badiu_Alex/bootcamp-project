package com.bootcamp.ddroidd.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JobListingDTO {

    private Integer id;

    private String position;

    private String description;

    private EmployerDTO employerDTO;

}
