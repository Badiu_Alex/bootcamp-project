package com.bootcamp.ddroidd.controller;

import com.bootcamp.ddroidd.dtos.ApplicantDTO;
import com.bootcamp.ddroidd.dtos.ApplicationDTO;
import com.bootcamp.ddroidd.dtos.EmployerDTO;
import com.bootcamp.ddroidd.dtos.JobListingDTO;
import com.bootcamp.ddroidd.services.ApplicantService;
import com.bootcamp.ddroidd.services.ApplicationService;
import com.bootcamp.ddroidd.services.EmployerService;
import com.bootcamp.ddroidd.services.JobListingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MainController {

    @Autowired
    private EmployerService employerService;
    @Autowired
    private JobListingService jobListingService;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private ApplicantService applicantService;

    @PostMapping("/employer")
    public Integer addEmployerPost(@RequestBody EmployerDTO employerDTO) {
        return employerService.createEmployer(employerDTO);
    }

    @PostMapping("/job-listing")
    public Integer addJobListingPost(@RequestBody JobListingDTO jobListingDTO) {
        return jobListingService.createJobListing(jobListingDTO);
    }

    @PostMapping("/application")
    public Integer addApplicationPost(@RequestBody ApplicationDTO applicationDTO) {
        return applicationService.createApplication(applicationDTO);
    }

    @PostMapping("/applicant")
    public void addApplicantPost(@RequestBody ApplicantDTO applicantDTO) {
        applicantService.createApplicant(applicantDTO);
    }

    @GetMapping("/job-listings/{employerId}")
    public List<JobListingDTO> jobListingsGet(@PathVariable("employerId") Integer employerId) {
        return jobListingService.getJobListingsByEmployerId(employerId);
    }

    @GetMapping("/applicants/employer/{employerId}")
    public List<ApplicantDTO> applicantsByEmployerIdGet(@PathVariable("employerId") Integer employerId) {
        return employerService.getApplicantsByEmployerId(employerId);
    }

    @GetMapping("/applicants/job-listing/{jobListingId}")
    public List<ApplicantDTO> applicantsByJobListingIdGet(@PathVariable("jobListingId") Integer jobListingId) {
        return jobListingService.getApplicantsByJobListingId(jobListingId);
    }

    @DeleteMapping("/job-listing/remove/{jobListingId}")
    public List<JobListingDTO> removeJobListingById(@PathVariable("jobListingId") Integer jobListingId) {
        return jobListingService.removeJobListingById(jobListingId);
    }
}
