package com.bootcamp.ddroidd;

import com.bootcamp.ddroidd.dtos.EmployerDTO;
import com.bootcamp.ddroidd.services.EmployerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdroiddApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdroiddApplication.class, args);
	}

}
