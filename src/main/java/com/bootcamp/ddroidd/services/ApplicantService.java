package com.bootcamp.ddroidd.services;

import com.bootcamp.ddroidd.dtos.ApplicantDTO;
import com.bootcamp.ddroidd.entities.Applicant;
import com.bootcamp.ddroidd.repository.ApplicantRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ApplicantService {

    @Autowired
    private ApplicantRepository applicantRepository;

    private ModelMapper modelMapper = new ModelMapper();

    public boolean isNotValidEmailAddress(String email) {
        String ePattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return !m.matches();
    }

    public Integer createApplicant(ApplicantDTO applicantDTO) {
        validateApplicant(applicantDTO);
        Applicant applicant = modelMapper.map(applicantDTO, Applicant.class);
        applicantRepository.save(applicant);
        return applicant.getId();
    }

    private void validateApplicant(ApplicantDTO applicantDTO) {
        if (applicantDTO.getEmail() == null || applicantDTO.getEmail().isBlank()) {
            throw new RuntimeException("Email field can not be empty!");
        }
        if (isNotValidEmailAddress(applicantDTO.getEmail())) {
            throw new RuntimeException("Please insert a valid email address!");
        }
        if (applicantRepository.findByEmail(applicantDTO.getEmail()).isPresent()) {
            throw new RuntimeException("Email already exists! Please user another email.");
        }
        if (applicantDTO.getPhone() == null || applicantDTO.getPhone().isBlank()) {
            throw new RuntimeException("Phone field can not be empty!");
        }
        if (applicantDTO.getCountry() == null || applicantDTO.getCountry().isBlank()) {
            throw new RuntimeException("Country field can not be empty!");
        }
        if (applicantDTO.getLastName() == null || applicantDTO.getLastName().isBlank()) {
            throw new RuntimeException("Last name field can not be empty!");
        }
        if (applicantDTO.getFirstName() == null || applicantDTO.getFirstName().isBlank()) {
            throw new RuntimeException("First name field can not be empty!");
        }
    }

}
