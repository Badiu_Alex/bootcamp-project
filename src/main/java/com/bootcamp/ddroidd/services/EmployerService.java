package com.bootcamp.ddroidd.services;

import com.bootcamp.ddroidd.dtos.ApplicantDTO;
import com.bootcamp.ddroidd.dtos.EmployerDTO;
import com.bootcamp.ddroidd.entities.Applicant;
import com.bootcamp.ddroidd.entities.Employer;
import com.bootcamp.ddroidd.repository.ApplicantRepository;
import com.bootcamp.ddroidd.repository.ApplicationRepository;
import com.bootcamp.ddroidd.repository.EmployerRepository;
import com.bootcamp.ddroidd.repository.JobListingRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class EmployerService {

    @Autowired
    private EmployerRepository employerRepository;

    @Autowired
    private JobListingRepository jobListingRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ApplicantRepository applicantRepository;

    private ModelMapper modelMapper = new ModelMapper();

    public Integer createEmployer(EmployerDTO employerDTO) {
        Employer employer = modelMapper.map(employerDTO, Employer.class);
        employerRepository.save(employer);
        return employer.getId();
    }

    public List<ApplicantDTO> getApplicantsByEmployerId(Integer employerId) {
        if (employerId == null || employerId <= 0) {
            return new LinkedList<>();
        }
        List<Integer> jobListingsIds = jobListingRepository.findJobListingsIDsByEmployerId(employerId);
        List<Integer> applicantsIds = applicationRepository.findApplicantsIdsByJobListingIds(jobListingsIds);
        List<Applicant> applicants = applicantRepository.findApplicantsByIds(applicantsIds);
        List<ApplicantDTO> applicantDTOS = new LinkedList<>();
        for (Applicant applicant : applicants) {
            ApplicantDTO applicantDTO = modelMapper.map(applicant, ApplicantDTO.class);
            applicantDTOS.add(applicantDTO);
        }
        return applicantDTOS;
    }
}
