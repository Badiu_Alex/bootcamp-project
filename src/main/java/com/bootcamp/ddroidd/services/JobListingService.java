package com.bootcamp.ddroidd.services;

import com.bootcamp.ddroidd.dtos.ApplicantDTO;
import com.bootcamp.ddroidd.dtos.JobListingDTO;
import com.bootcamp.ddroidd.entities.Applicant;
import com.bootcamp.ddroidd.entities.JobListing;
import com.bootcamp.ddroidd.repository.ApplicantRepository;
import com.bootcamp.ddroidd.repository.ApplicationRepository;
import com.bootcamp.ddroidd.repository.JobListingRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class JobListingService {

    @Autowired
    private JobListingRepository jobListingRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ApplicantRepository applicantRepository;

    private ModelMapper modelMapper = new ModelMapper();

    public Integer createJobListing(JobListingDTO jobListingDTO) {
        if (jobListingRepository.findByPosition(jobListingDTO.getPosition()).isPresent()) {
            throw new RuntimeException("This position has already been posted!");
        }
        TypeMap<JobListing, JobListingDTO> propertyMapper = modelMapper.getTypeMap(JobListing.class, JobListingDTO.class);
        if (propertyMapper == null) {
            propertyMapper = modelMapper.createTypeMap(JobListing.class, JobListingDTO.class);
        }
        propertyMapper.addMappings(
                mapper -> mapper.map(src -> src.getEmployer().getName(), JobListingDTO::setEmployerDTO)
        );
        JobListing jobListing = modelMapper.map(jobListingDTO, JobListing.class);
        jobListingRepository.save(jobListing);
        return jobListing.getId();
    }

    public List<JobListingDTO> getJobListingsByEmployerId(Integer employerId) {
        if (employerId == null || employerId <= 0) {
            return new LinkedList<>();
        }
        List<JobListing> jobListingsByEmployerId = jobListingRepository.findAllByEmployer_Id(employerId);
        List<JobListingDTO> jobListingDTOS = new LinkedList<JobListingDTO>();
        TypeMap<JobListing, JobListingDTO> propertyMapper = modelMapper.getTypeMap(JobListing.class, JobListingDTO.class);
        if (propertyMapper == null) {
            propertyMapper = modelMapper.createTypeMap(JobListing.class, JobListingDTO.class);
        }
        propertyMapper.addMappings(
                mapper -> mapper.map(JobListing::getEmployer, JobListingDTO::setEmployerDTO)
        );
        for (JobListing jobListing : jobListingsByEmployerId) {

            JobListingDTO jobListingDTO = modelMapper.map(jobListing, JobListingDTO.class);
            jobListingDTOS.add(jobListingDTO);
        }
        return jobListingDTOS;
    }

    public List<ApplicantDTO> getApplicantsByJobListingId(Integer jobListingId) {
        if (jobListingId == null || jobListingId <= 0) {
            return new LinkedList<>();
        }
        List<Integer> applicantsIdsByJobListingId = applicationRepository.findApplicantsIdsByJobListingId(jobListingId);
        List<Applicant> applicants = applicantRepository.findApplicantsByIds(applicantsIdsByJobListingId);
        List<ApplicantDTO> applicantDTOS = new LinkedList<>();
        for (Applicant applicant : applicants) {
            ApplicantDTO applicantDTO = modelMapper.map(applicant, ApplicantDTO.class);
            applicantDTOS.add(applicantDTO);
        }
        return applicantDTOS;
    }

    public List<JobListingDTO> removeJobListingById(Integer jobListingId) {
        Integer employerId = jobListingRepository.findById(jobListingId).get().getEmployer().getId();
        jobListingRepository.deleteById(jobListingId);
        return getJobListingsByEmployerId(employerId);
    }
}
