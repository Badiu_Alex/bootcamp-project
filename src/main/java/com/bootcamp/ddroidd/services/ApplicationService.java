package com.bootcamp.ddroidd.services;

import com.bootcamp.ddroidd.dtos.ApplicationDTO;
import com.bootcamp.ddroidd.entities.Applicant;
import com.bootcamp.ddroidd.entities.Application;
import com.bootcamp.ddroidd.entities.JobListing;
import com.bootcamp.ddroidd.repository.ApplicantRepository;
import com.bootcamp.ddroidd.repository.ApplicationRepository;
import com.bootcamp.ddroidd.repository.JobListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationService {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ApplicantRepository applicantRepository;

    @Autowired
    private JobListingRepository jobListingRepository;

    public Integer createApplication(ApplicationDTO applicationDTO) {
        if (applicationDTO.getApplicantId() == null || applicationDTO.getApplicantId() <= 0) {
            throw new RuntimeException("Invalid applicant ID!");
        }
        if (applicationDTO.getJobListingId() == null || applicationDTO.getJobListingId() <= 0) {
            throw new RuntimeException("Invalid job listing ID!");
        }
        List<Integer> ids = applicationRepository.findApplicationsIdsByApplicantIdAndJobListingId(applicationDTO.getApplicantId(), applicationDTO.getJobListingId());
        if (!ids.isEmpty()) {
            throw new RuntimeException("You have already applied for this job!");
        }
        Applicant applicant = applicantRepository.findById(applicationDTO.getApplicantId()).get();
        JobListing jobListing = jobListingRepository.findById(applicationDTO.getJobListingId()).get();

        Application application = new Application(applicant, jobListing);
        applicationRepository.save(application);
        return application.getId();
    }

}
