# Bootcamp project


## Application Properties Setup

Database source is correlated with the local machine and will try to connect to jdbc:mysql://localhost:3306/ddroidd
and if the database is not created the application will create a database named ddroidd based on the entities.

The credentials of the connection are setup as general for now and need to be adapted for every person who will try to
run this application. As a good practice please change those properties according to your local machine and setup.
